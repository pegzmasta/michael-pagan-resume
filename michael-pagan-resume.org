# -*- Mode: Org -*-
#+TITLE: *\dotfill\hspace{0cm} Michael Pagan | Remote Sales Representative \dotfill*
#+DATE: 6 August 2017
#+AUTHOR: Michael Pagan
#+SUBTITLE: \vspace{-0.8ex}
#+SUBTITLE: \textcolor{gray}{\faMobile*}\nbsp 423-812-1676 \textbar{}
#+SUBTITLE: \textcolor{gray}{\faEnvelope}\nbsp [[mailto:michael.pagan@member.fsf.org][{{{verify(michael.pagan@member.fsf.org)}}}]] \textbar{}
#+SUBTITLE: \textcolor{gray}{\faLinkedin}\nbsp [[https://www.linkedin.com/in/mike-pagan-x/][{{{verify(linkedin.com/in/mike-pagan-x)}}}]] \textbar{}
#+SUBTITLE: \textcolor{gray}{\faMapMarker*}\nbsp Greeneville, TN
#+OPTIONS: toc:nil date:nil author:nil H:10 tex:t
#+STARTUP: showeverything hidestars indent
#+KEYWORDS: data-entry, develop, documenting, free software, GNU, hacking, HTML, itinerary planning, letter writing, logistics support, operating systems,
#+KEYWORDS: programming, record keeping, research, schedule, shell-scripting, transcription, typing, web
#+DESCRIPTION: Michael Pagan's resume for landing an interview for a job
#+MACRO: verify @@latex:\textcolor{coolblack}{$1}@@
#+MACRO: hidden @@latex:\textcolor{gray}{$1}@@
#+LATEX_CLASS_OPTIONS: [letterpaper]
#+LATEX_HEADER: \usepackage{mycv}% my custom resume template
#+LATEX_HEADER: \usepackage[dvipsnames]{xcolor}
#+LATEX_HEADER: \usepackage{color, soul}
#+LATEX_HEADER: \usepackage{titling}% for manipulating the title
#+LATEX_HEADER: \usepackage{enumitem}
#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \usepackage{pbsi}
#+LATEX_HEADER: \usepackage{pifont}
#+LATEX_HEADER: \usepackage[T1]{fontenc}
#+LATEX_HEADER: \usepackage{fontawesome5}
#+LATEX_HEADER: \usepackage{tcolorbox}
#+LATEX_HEADER: \usepackage[letterspace=50]{microtype}% Applying this to section headlines through latex stylesheet
#+LATEX_HEADER: \usepackage[margin=0.25in, nosep]{geometry}% To allow for more information
#+LATEX_HEADER: \setlist{leftmargin=0.25in, nosep}
#+LATEX_HEADER: \thispagestyle{fancy}
#+LATEX_HEADER: \fancyhf{} % sets both header and footer to nothing
#+LATEX_HEADER: \renewcommand{\headrulewidth}{0pt}
#+LATEX_HEADER: \pagestyle{fancy}
#+LATEX_HEADER: \setlength{\droptitle}{-50pt}% Remove excess verticle space above the title
#+LATEX_HEADER: \definecolor{my-gray}{gray}{0.90}
#+LATEX_HEADER: \definecolor{coolblack}{rgb}{0.0, 0.18, 0.39}
#+LATEX: \makeatletter\let\ps@plain\ps@fancy\makeatother
#+LATEX: \renewcommand{\labelitemii}{$\blacklozenge$}
#+LATEX_HEADER: \newcommand{\mybox}[1]{%
#+LATEX_HEADER:   \setbox0=\hbox{#1}%
#+LATEX_HEADER:   \setlength{\textwidth}{\dimexpr\wd0+13pt}%
#+LATEX_HEADER:   \begin{tcolorbox}[colframe=black,colback=my-gray,boxrule=0.5pt,arc=4pt,
#+LATEX_HEADER:       left=4pt,right=4pt,top=4pt,bottom=4pt,boxsep=0pt,width=\textwidth,box align=top]
#+LATEX_HEADER:     #1
#+LATEX_HEADER:   \end{tcolorbox}
#+LATEX_HEADER: }
#+LATEX: \setul{}{1.555pt}
#+LATEX: \setulcolor{gray}
#+LATEX: \vspace{-17.5ex}% Removing excess space below title, so that I can add more information
#+LATEX: \rule[1ex]{1.002\textwidth}{2.5pt}
#+LATEX: \vspace{-6.2ex}
* \textcolor{my-gray}{.}\faUserCheck\textcolor{my-gray}{.}\hfill{SUMMARY: VERIFIED SKILLS}\hfill\textcolor{my-gray}{.}\faUserCheck\textcolor{my-gray}{.}
\vspace{-2.4ex}
** _Scored 17\times via /[[https://resumes.indeed.com/resume/8825af95147a99e4#footer-locale-selection-label:~:text=fallback][{{{verify(Indeed Assessments \textcolor{gray}{\faExternalLink*})}}}]]/ (scroll to footer or see bottom of [[https://gitlab.com/pegzmasta/michael-pagan-resume/-/raw/master/resumes.indeed.com_resume_8825af95147a99e4.png][{{{verify(screenshot \textcolor{gray}{\faExternalLink*})}}}]]):_
*** 10/2019 to 11/2022
**** @@latex:\colorbox{my-gray}{\textcolor{gray}{Expert}}@@
***** \textcolor{gray}{\faStar\faStar\faStar\faStar\faStar}
\bullet\nbsp Typing
\bullet\nbsp Data entry
\bullet\nbsp Data accuracy
\bullet\nbsp Reliability, [[Quality Analyst:][{{{hidden(\faBookmark)}}}]]{{{hidden(—)}}}@@latex:\raisebox{0.4cm}{\mybox{Implemented continuous improvement strategies to enhance efficiency;}}@@
**** @@latex:\colorbox{my-gray}{\textcolor{gray}{Highly Proficient}}@@
***** \textcolor{gray}{\faStar\faStar\faStar\faStar\faStar[regular]}
\bullet\nbsp Work motivation
\bullet\nbsp Basic computer skills
\bullet\nbsp Written communication
\bullet\nbsp Call center, [[Customer Service Representative/Mentor:][{{{hidden(\faBookmark)}}}]]{{{hidden(—)}}}@@latex:\raisebox{0.4cm}{\mybox{Handled issues, escalations, and repairs;}}@@
\bullet\nbsp Advanced awareness
\bullet\nbsp Customer service
\bullet\nbsp Detailedness
\bullet\nbsp Excel spreadsheets, [[Implemented 63% training and 100^{+} logbook editing to validate operations.][{{{hidden(\faBookmark)}}}]]{{{hidden(—)}}}@@latex:\raisebox{0.4cm}{\mybox{Developed Excel macros and system scripts;}}@@
**** @@latex:\colorbox{my-gray}{\textcolor{gray}{Proficient}}@@
***** \textcolor{gray}{\faStar\faStar\faStar\faStar[regular]\faStar[regular]}
\vspace{0.55ex}
\bullet\nbsp Customer focus\nbsp
\bullet\nbsp \ul{Administrative assistant}
\bullet\nbsp Sales skills
\bullet\nbsp \ul{Inside sales}
\bullet\nbsp Proofreading, [[Quality Analyst:][{{{hidden(\faBookmark)}}}]]{{{hidden(——)}}}@@latex:\begin{flushright}\vspace{-5.15ex}\raisebox{0.4cm}{\mybox{Promoted the \textit{Apple Style Guide}.}}\end{flushright}\newline@@
\vspace{-2.695ex}\hspace{6.8cm} [[7041 in USMC Reserve:][{{{hidden(\faLevelDown*)}}}]] \hspace{3.05cm} [[Regional Account Manager:][{{{hidden(\faUps)}}}]]
\vspace{2.02ex}
* \textcolor{my-gray}{.}\faBriefcase\textcolor{my-gray}{.}\hfill{PROFESSIONAL EXPERIENCE}\hfill\textcolor{my-gray}{.}\faBriefcase\textcolor{my-gray}{.}
\vspace{-2.4ex}
** _Package Express Center, Inc. — Greeneville, TN_
*** 12/2022 to 08/2023
**** Regional Account Manager:
***** Converted UPS pipeline in CRM, closing 47 B2B sales, and achieved 3^{rd} best performance on team.
- Enhanced CRM with 10-stage, sales pipeline, Excel [[https://www.linkedin.com/in/michael-pagan-4607a7249/details/experience/1635547978593/single-media-viewer/?profileId=ACoAAD2E_8gBQ07m0Y3spth8n_k-vSKKpa_Lcm8][{{{verify(game \textcolor{gray}{\faExternalLink*})}}}]] to amplify sales strategy, increasing commissions 15%.\textcolor{lightgray}{\hrule}
#+COMMENT: 40 (quota after 8 months) \div 47 (sales for the year) = 15%
**** @@latex:\colorbox{my-gray}{\textcolor{gray}{\faLightbulb\hspace{-0.05cm}—Additional skills:}}\textcolor{white}{.}@@
\vspace{-1.65ex}\textcolor{lightgray}{\hrule}
***** \textcolor{gray}{Building relationships, closing skills, regional sales, key account management, value proposition development.}
\vspace{3.63ex}
** _Foundever/Sitel Group — Knoxville, TN [Remote]_
*** 08/2021 to 08/2022
**** Quality Analyst:
***** Eliminated 5 wastes with 10-module, QA email/user feedback, workflow/macOS [[https://www.linkedin.com/in/michael-pagan-4607a7249/details/experience/2025139826/multiple-media-viewer/?profileId=ACoAAD2E_8gBQ07m0Y3spth8n_k-vSKKpa_Lcm8&treasuryMediaId=1635547967671][{{{verify(automation \textcolor{gray}{\faExternalLink*})}}}]].
- Developed 95-slide, interactive, troubleshooting [[https://www.linkedin.com/in/michael-pagan-4607a7249/details/experience/2025139826/multiple-media-viewer/?profileId=ACoAAD2E_8gBQ07m0Y3spth8n_k-vSKKpa_Lcm8&treasuryMediaId=1635547964751][{{{verify(presentation \textcolor{gray}{\faExternalLink*})}}}]] devised from 3 knowledge base systems after Zoom meeting.
- Invented Excel VBA, phone quality [[https://www.linkedin.com/in/michael-pagan-4607a7249/details/experience/2025139826/multiple-media-viewer/?profileId=ACoAAD2E_8gBQ07m0Y3spth8n_k-vSKKpa_Lcm8&treasuryMediaId=1635539380438][{{{verify(automation \textcolor{gray}{\faExternalLink*})}}}]] of 8 attributes with 6 analysis controls to analyze database of user feedback.
- Created PowerPoint screen sharing visual [[https://www.linkedin.com/in/michael-pagan-4607a7249/details/experience/2025139826/multiple-media-viewer/?profileId=ACoAAD2E_8gBQ07m0Y3spth8n_k-vSKKpa_Lcm8&treasuryMediaId=1635539378831][{{{verify(guide \textcolor{gray}{\faExternalLink*})}}}]] with 34 knowledge base links and [[https://www.linkedin.com/in/michael-pagan-4607a7249/details/experience/2025139826/multiple-media-viewer/?profileId=ACoAAD2E_8gBQ07m0Y3spth8n_k-vSKKpa_Lcm8&treasuryMediaId=1635548608544][{{{verify(FAQ \textcolor{gray}{\faExternalLink*})}}}]], promoting 100% root cause analysis.\textcolor{lightgray}{\hrule}\vspace{-1.051ex}
** @@latex:\colorbox{my-gray}{\textcolor{gray}{Foundever/Sitel Group — Knoxville, TN [Remote]}}@@
\vspace{-1.5ex}\textcolor{lightgray}{\hrule}
*** \textcolor{gray}{02/2021 to 07/2021}
**** Customer Service Representative/Mentor:
***** Authored knowledge base [[https://www.linkedin.com/in/michael-pagan-4607a7249/details/experience/1635550656871/single-media-viewer/?profileId=ACoAAD2E_8gBQ07m0Y3spth8n_k-vSKKpa_Lcm8][{{{verify(documentation \textcolor{gray}{\faExternalLink*})}}}]] which enhanced operations of 10^{+} mentors.
- Mentored 20^{+} iOS Tier 1 Advisors through Adobe Connect customer facing training curriculum, improving customer success.
\vspace{3.63ex}
** _USMC Military Transition — Greeneville, TN_
*** 06/2015 to 02/2021
**** Transitioned Veteran:
***** Facilitated [[https://www.linkedin.com/in/michael-pagan-4607a7249/details/experience/#:~:text=Line%20-,Production&text=Lines%20%C2%B7%20Line%20-,Production&text=Machines%20%C2%B7%20Line-,Production][{{{verify(3-year \textcolor{gray}{\faExternalLink*})}}}]] warehouse assembly/production; afterward, experienced 1-year in customer service.
**** 7041 in USMC Reserve:
***** Devised informative policies (Word) and help articles ([[https://www.linkedin.com/in/michael-pagan-4607a7249/details/experience/2025148442/multiple-media-viewer/?profileId=ACoAAD2E_8gBQ07m0Y3spth8n_k-vSKKpa_Lcm8&treasuryMediaId=1635550657652][{{{verify(PowerPoint \textcolor{gray}{\faExternalLink*})}}}]]), training 12^{+} Marines 4\times monthly.
\vspace{3.63ex}
** _United States Marine Corps (USMC) — MCAS New River, NC_
*** [[https://gitlab.com/pegzmasta/michael-pagan-resume/-/raw/master/military-related/michael-pagan-usmc-DD214.pdf][{{{verify(05/2011 to 05/2015 \textcolor{gray}{\faExternalLink*})}}}]]
**** Aviation Operations Specialist (AOS)/[[https://gitlab.com/pegzmasta/michael-pagan-resume/-/raw/master/military-related/michael-pagan-usmc-DD2586.pdf][{{{verify(7041 \textcolor{gray}{\faExternalLink*})}}}]]:
#+COMMENT: Out the 8 ground training events performed (i.e. BCP, PFT, CFT, Swim Qual. MCMAP, NBC/CBRN, Rifle, Pistol), I only administered 5.
#+COMMENT: gawk 'BEGIN { print 5/8 * 100 }'
***** Implemented 63% training and 100^{+} logbook editing to validate operations.
- Pioneered 2 comprehensive VBA [[https://gitlab.com/explore?name=United+States+Marine+Corps][{{{verify(macros \textcolor{gray}{\faExternalLink*})}}}]] that reduced 120-minute, data entry, spreadsheet processes by a whopping 95%.
\vspace{3.63ex}
* \textcolor{my-gray}{.}\faCertificate\textcolor{my-gray}{.}\hfill{LICENSES \& CERTIFICATIONS}\hfill\textcolor{my-gray}{.}\faCertificate\textcolor{my-gray}{.}
\vspace{-2.4ex}
** Insurance Producer — License N\deg, [[https://sbs.naic.org/solar-external-lookup/lookup/licensee/summary/3002092237?jurisdiction=TN&entityType=IND&licenseType=PRO][{{{verify(3002092237 \textcolor{gray}{\faExternalLink*})}}}]]; Six Sigma Green Belt — [[https://badgr.com/public/assertions/QO3ZdidLSruLgXwp0Rthkw?identity__email=michael.pagan@member.fsf.org][{{{verify(Verified Badge \textcolor{gray}{\faExternalLink*})}}}]]; Professional Typist — [[https://typinglessonsfree.com/cert/view/211527/20929][{{{verify(60 WPM \textcolor{gray}{\faExternalLink*})}}}]]
\vspace{3.63ex}
* \textcolor{my-gray}{.}\faGraduationCap\textcolor{my-gray}{.}\hfill{EDUCATION}\hfill\textcolor{my-gray}{.}\faGraduationCap\textcolor{my-gray}{.}
\vspace{-2.4ex}
** _Marine Corps University — Quantico, VA_
*** 06/2016
**** Corporal of Marines in Military and Strategic Leadership | [[https://gitlab.com/pegzmasta/michael-pagan-resume/-/raw/master/military-related/michael-pagan-usmc-JST.pdf][{{{verify(Acquired 32 total credit hours \textcolor{gray}{\faExternalLink*})}}}]]
\vspace{3.63ex}
**** COMMENT [[https://gitlab.com/pegzmasta/michael-pagan-resume/-/raw/master/military-related/michael-pagan-usmc-JST.pdf][{{{verify(Related Coursework: \textcolor{gray}{\faExternalLink*})}}}]]
***** 1—Corporals Course; 2—Nuclear, Biological, Chemical; 3—Marine AOS Course; 4—Recruit Training Master.
* \textcolor{my-gray}{.}\faHeart\textcolor{my-gray}{.}\hfill{INTERESTS}\hfill\textcolor{my-gray}{.}\faHeart\textcolor{my-gray}{.}
\vspace{-2.4ex}
** /The Instant Millionaire/, by Mark Fisher; /Atomic Habits/, by James Clear; hot sauce, like /Salsa Valentina/; mangos & guacamole; /Nausicaä of the Valley of the Wind/, a Studio Ghibli film; /The Legend of Zelda: A Link to the Past/ & /Chrono Trigger/, by Nintendo; /TurboAnt V8 E-Scooter/; [[https://gitlab.com/pegzmasta/michael-pagan-resume/-/raw/master/military-related/Ziplining_Triforce_2017.png][{{{verify(ziplining \textcolor{gray}{\faExternalLink*})}}}]]; obstacle courses & marathons (was [[https://gitlab.com/pegzmasta/michael-pagan-resume/-/raw/master/military-related/Tough_Mudder_26292_in_2017.jpg][{{{verify(Tough Mudder 26292 \textcolor{gray}{\faExternalLink*})}}}]] in 2017); chess; GNU/Linux
